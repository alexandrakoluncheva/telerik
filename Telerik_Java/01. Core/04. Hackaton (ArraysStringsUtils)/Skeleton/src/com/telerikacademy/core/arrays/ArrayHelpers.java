package com.telerikacademy.core.arrays;

@SuppressWarnings({"ManualArrayCopy", "ExplicitArrayFilling"})
public class ArrayHelpers {

    /**
     * Adds <code>element</code> to the end of <code>source.</code> array.
     *
     * @param source  int[] - The array to add to
     * @param element int - The element to add
     * @return int[] - A new array, the original array with element at the end
     * @author Alexandra Koluncheva
     */

    public static int[] add(int[] source, int element) {

        int[] arrayResult = new int[source.length + 1];

        for (int i = 0; i < arrayResult.length; i++) {
            if (i < source.length) {
                arrayResult[i] = source[i];
            } else {
                arrayResult[i] = element;
            }
        }
        return arrayResult;
    }

    /**
     * Adds <code>element</code> to the start of <code>source.</code> array.
     *
     * @param source  int[] - The array to add to
     * @param element int - The element to add
     * @return int[] - A new array, the original array with element at the start.
     * @author Alexandra Koluncheva
     */

    public static int[] addFirst(int[] source, int element) {

        int[] arrayResult = new int[source.length + 1];

        for (int i = 0; i < arrayResult.length; i++) {


            if (i == 0) {
                arrayResult[i] = element;
            } else {
                arrayResult[i] = source[i - 1];
            }
        }

        return arrayResult;
    }

    /**
     * Method add array of items a <code>elements</code> to initial other array <code>source</code>.
     *
     * @param source   Main array
     * @param elements Other array of elements, that should be added to main array
     * @return New array, that contains concatenated two arrays source and elements
     * @author Vladislav Nikolchov
     */
    public static int[] addAll(int[] source, int... elements) {
        int[] arrayResult = new int[source.length + elements.length];
        for (int i = 0; i < arrayResult.length; i++) {
            if (i < source.length) {
                arrayResult[i] = source[i];
            } else {
                arrayResult[i] = elements[i - source.length];
            }
        }
        return arrayResult;
    }

    /**
     * Method checks whether a array <code>source</code> contain integer <code>element</code>.
     *
     * @param source  Main integer array
     * @param element The integer to be searched for
     * @return Indicating whether a integer exist in the specified integer array:
     * @author Vladislav Nikolchov
     */

    public static boolean contains(int[] source, int element) {
        boolean result = false;
        for (int i : source) {
            if (i == element) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Method take array <code>source</code> and copied to <code>destinationArray</code> with specified length.
     *
     * @param sourceArray      Main integer array
     * @param destinationArray copied integer array
     * @param count            how many elements to copy
     * @author Vladimir Seleznyov
     */
    public static void copy(int[] sourceArray, int[] destinationArray, int count) {
        for (int i = 0; i < count; i++) {
            if (sourceArray.length <= i) destinationArray[i] = 0;
            else destinationArray[i] = sourceArray[i];
        }
    }

    /**
     * Fill the provided single int array with the provided int values.
     *
     * @param source  - Provide an int array to be filled in with elements
     * @param element - Provide element to fill in the array.
     *                <p>
     *                Author: @Georgi Georgiev
     **/
    public static void fill(int[] source, int element) {
        int size = source.length;

        for (int i = 0; i < size; i++) {
            source[i] = element;
        }

    }


    /**
     * Method take array <code>source</code> and copied to <code>destinationArray</code> with specified length.
     *
     * @param sourceArray      Main integer array
     * @param sourceStartIndex start position for sourceArray
     * @param destinationArray copied integer array
     * @param destStartIndex   start position for destinationArray
     * @param count            how many elements to copy
     * @author Vladimir Seleznyov
     */
    public static void copyFrom(int[] sourceArray, int sourceStartIndex, int[] destinationArray, int destStartIndex, int count) {
        for (int i = 0; i < count; i++) {
            destinationArray[destStartIndex + i] = sourceArray[sourceStartIndex + i];
        }
    }

    /**
     * Find the first time an int input value occurs in a given int array.
     *
     * @param source - The array we are looking for the target.
     * @param target - The value that is being searched for.
     * @return - The position in the array of the target. (Keep in mind the first position is 0!). If target is not
     * found, the return is -1.
     * <p>
     * Author: @Georgi Georgiev
     */
    public static int firstIndexOf(int[] source, int target) {

        int size = source.length;
        int result = -1;
        for (int i = 0; i < size; i++) {
            if (source[i] == target) {
                result = i;
                break;
            }
        }
        return result;
    }


    /**
     * Inserts a new element in a given position in an array.
     *
     * @param source  The original array
     * @param index   The position to insert the element
     * @param element The value of the element to be inserted
     * @return A new array with the inserted element.
     * @author Georgi Nikolov
     */
    public static int[] insert(int[] source, int index, int element) {
        int[] result = new int[source.length + 1];
        for (int i = 0; i < index; i++) {
            result[i] = source[i];
        }
        result[index] = element;
        for (int i = index + 1; i < result.length; i++) {
            result[i] = source[i - 1];
        }

        return result;
    }


    /**
     * Checks if a given index is in the borders of a given string.
     *
     * @param source The array to be checked in
     * @param index  The index to check
     * @return Returns true if the index is in the borders of the array, and false if it is not.
     * @author Georgi Nikolov
     */
    public static boolean isValidIndex(int[] source, int index) {

        return index >= 0 && index < source.length;
    }


    /**
     * finds the last index of <code>target</code> within <code>source</code>
     * <p>
     * example: <code>lastIndexOf(new int[]{1, 2, 3, 4, 2}, 2);</code>
     * //returns 4
     *
     * @param source - array checked by method
     * @param target - element looked for by method
     * @return - last index of <code>target</code> within <code>source</code>, otherwise -1
     * @author Dimitar Simeonov
     */

    public static int lastIndexOf(int[] source, int target) {

        int lastPosition = -1;

        for (int index = 0; index < source.length; index++) {

            if (source[index] == target) lastPosition = index;

        }

        return lastPosition;

    }

    /**
     * removes all occurrences of <code>element</code> within <code>source</code>
     * <p>
     * example: <code>removeAllOccurrences(new int[]{1, 2, 3, 4, 2}, 2);</code>
     * //returns {1, 3, 4}
     *
     * @param source  - array to remove from
     * @param element - element to be removed
     * @return - new array with all occurrences of <code>element</code> removed
     * @author Dimitar Simeonov
     */

    public static int[] removeAllOccurrences(int[] source, int element) {

        int sizeResultArray = source.length;
        for (int indexSourceArray = 0; indexSourceArray < source.length; indexSourceArray++) {

            if (source[indexSourceArray] == element) sizeResultArray--;

        }

        int lastAddedSymbol = 0;
        int[] resultArray = new int[sizeResultArray];
        for (int indexResultArray = 0; indexResultArray < sizeResultArray; indexResultArray++) {

            for (int indexOfSourceArray = lastAddedSymbol; indexOfSourceArray < source.length; indexOfSourceArray++) {

                lastAddedSymbol++;

                if (source[indexOfSourceArray] != element) {

                    resultArray[indexResultArray] = source[indexOfSourceArray];
                    break;

                }

            }

        }

        return resultArray;

    }


    /**
     * This method reverses arrayToReverse. Example: {1,2,3} becomes {3,2,1}
     *
     * @param arrayToReverse the array you wish to reverse
     * @author Ilhan Konduzov
     */

    public static void reverse(int[] arrayToReverse) {
        int size = arrayToReverse.length;

        int[] rev = new int[size];

        int revIndx = 0;

        for (int i = size - 1; i >= 0; i--) {
            rev[revIndx] = arrayToReverse[i];
            revIndx++;
        }

        for (int i = 0; i < size; i++) {
            arrayToReverse[i] = rev[i];
        }
    }

    /**
     * Returns a new array, from source, starting from startIndex and until endIndex
     *
     * @param source     The array to create the new array from
     * @param startIndex The starting index
     * @param endIndex   The end index
     * @return A new array starting from startIndex and until endIndex
     * @author Ilhan Konduzov
     */

    public static int[] section(int[] source, int startIndex, int endIndex) {
        int size = 0;

        if (endIndex > source.length) {
            endIndex = source.length - 1;
        }

        if (startIndex < 0 || startIndex > source.length) {
            startIndex = 0;
            endIndex = source.length - 1;
        }

        if (startIndex == 0) {
            size = endIndex + 1;
        } else {
            size = endIndex - startIndex + 1;
        }

        int[] newArray = new int[size];
        int counter = 0;

        for (int i = startIndex; i <= endIndex; i++) {
            newArray[counter] = source[i];
            counter++;
        }
        return newArray;
    }

}
