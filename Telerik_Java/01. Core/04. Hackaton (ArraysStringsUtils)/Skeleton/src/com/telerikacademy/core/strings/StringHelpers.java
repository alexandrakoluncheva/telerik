package com.telerikacademy.core.strings;

@SuppressWarnings("StringConcatenationInLoop")
public class StringHelpers {

    /**
     * Abbreviate <code>source</code> to a string with <code>maxLength</code>.
     *
     * @param source    The initial text you want to abbreviate
     * @param maxLength the length you want your text to be
     * @return An abbreviated string
     * @author Alexandra Koluncheva
     */

    public static String abbreviate(String source, int maxLength) {

        char array[]= new char[maxLength];

            if (source.length() <= maxLength) {
                return source;
            }

            else {
                char ch[] = source.toCharArray();
                int size=source.length();
                for (int i=0; i<maxLength; i++){
                    char a= ch[i];
                    array[i]=a;
                }
            }

            String str = new String(array);
            return str + "...";
    }


    /**
     * Capitalize <code>source</code>
     *
     * @param source The text you want to capitalize
     * @return A string with all capital letters
     * @author Alexandra Koluncheva
     */

    public static String capitalize(String source) {

        char ch[] = source.toCharArray();

        for (int i = 0; i < source.length(); i++) {

            if (i == 0 && ch[i] != ' ' || ch[i] != ' ' && ch[i - 1] == ' ') {

                if (ch[i] >= 'a' && ch[i] <= 'z') {
                    ch[i] = (char) (ch[i] - 'a' + 'A');
                }

            } //else if (ch[i] >= 'A' && ch[i] <= 'Z') {
               // ch[i] = (char) (ch[i] + 'a' - 'A');
            //}
        }
        String st = new String(ch);
        return st;
    }
    /**
     * Concatenates <code>string1</code> to the end of <code>string2</code>.
     *
     * @param string1 The left part of the new string
     * @param string2 The right part of the new string
     * @return A string that represents the concatenation of string1 followed by string2's characters.
     * @author Vladislav Nikolchov
     */

    public static String concat(String string1, String string2) {
        String result;
        result = string1 + string2;
        return result;
    }

    /**
     * Method checks whether a <code>source</code> contain character <code>symbol</code>.
     *
     * @param source Source, where is searching
     * @param symbol The characters to be searched for|
     * @return Indicating whether a char exist in the specified string:
     * Example:
     * String myStr = "Hello";
     * System.out.println(myStr.contains("Hel"));   // true
     * @author Vladislav Nikolchov
     */
    public static boolean contains(String source, char symbol) {
        char[] arrayChar =  source.toCharArray();
        boolean result = false;
        for (int i = 0; i < source.length(); i++) {
            if (Character.compare(arrayChar[i],symbol)==0) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Method checks whether the <code>source</code> ends with a specified <code>target</code>.
     *
     * @param source Source, where is searching
     * @param target The character we want to string ends
     * @return true or false if the string end with a specified char
     *
     * @author Vladimir Seleznyov
     */
    public static boolean endsWith(String source, char target) {
        if(source.length() == 0) return false;
        char[] sourceArray = source.toCharArray();
        return sourceArray[sourceArray.length - 1] == target;
    }

    /**
     * Method checks whether the <code>source</code> ends with a specified <code>target</code>.
     *
     * @param source Source, where is searching
     * @param target The characters to be searched for
     * @return the address of the first target char
     *
     * @author Vladimir Seleznyov
     */
    public static int firstIndexOf(String source, char target) {
        char[] sourceArray = source.toCharArray();
        for (int i = 0; i < sourceArray.length; i++) {
            if (sourceArray[i] == (target)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Find the last instance of char in a string.
     *
     * @param source - The string that is looked into.
     * @param symbol - The char that is searched for.
     * @return the index of the last place the char occurs or -1 if there is no match.
     * <p>
     * Author: @Georgi Georgiev
     */
    public static int lastIndexOf(String source, char symbol) {

        int result = -1;
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbol) {
                result = i;
            }
        }
        return result;
    }

    /**
     * Pads a string on both sides with input symbol in order to achieve desired length of the String.
     *
     * @param source        - String to be padded.
     * @param length        - Length of the string to achieve.
     * @param paddingSymbol - Symbol to add to achieve length.
     * @return - The String with desired length, padded if necessary.
     * <p>
     * Author: @Georgi Georgiev
     */
    public static String pad(String source, int length, char paddingSymbol) {


        int additional = length - source.length();
        if (length - source.length() > 0) {
            if (additional % 2 != 0) {
                length--;
            }
            source = padEnd(source, length - additional / 2, paddingSymbol);
            source = padStart(source, length, paddingSymbol);
        }

        return source;

    }

    /**
     * Pads the end of the string with a given symbol until it reaches a given length.
     *
     * @param source        The string to be padded
     * @param length        The length the symbol has to reach after padding
     * @param paddingSymbol The symbol the string has to be padded with
     * @return Returns the padded string.
     * @author Georgi Nikolov
     */
    public static String padEnd(String source, int length, char paddingSymbol) {

        int interactions = length - source.length();

        for (int i = 0; i < interactions; i++) {
            source = source + paddingSymbol;
        }
        return source;
    }

    /**
     * Pads the start of the string with a given symbol until it reaches a given length.
     *
     * @param source        The string to be padded
     * @param length        The length the symbol has to reach after padding
     * @param paddingSymbol The symbol the string has to be padded with
     * @return Returns a new padded string.
     * @author Georgi Nikolov
     */
    public static String padStart(String source, int length, char paddingSymbol) {

        int interactions = length - source.length();
        String newString = "";

        if (source.length() > length) {
            return source;
        }

        if (source.length() != 0) {
            for (int i = 0; i < interactions; i++) {
                newString = newString + paddingSymbol;
            }
            newString = newString + source;
        }

        return newString;
    }


    /**
     * repeats a given string <code>times</code> times
     *
     * example: <code>repeat("Java", 3);</code>
     * //returns JavaJavaJava
     *
     * @param source - string to be repeated
     * @param times - how many times is the above string repeated
     * @return - repeated string
     *
     * @author Dimitar Simeonov
     */

    public static String repeat(String source, int times) {

        String result = source;
        for (int i = 1; i < times; i++) {

            result = result + source;

            }

        return result;

    }

    /**
     * reverses <code>source</code> so that first element becomes last, second becomes second to last, and so on
     *
     * example: <code>reverse("Java");</code>
     * //returns avaJ
     *
     * @param source - string to be reversed
     * @return - reversed string
     *
     * @author Dimitar Simeonov
     */

    public static String reverse(String source) {

        String result = "";
        for (int index = source.length()-1; index >= 0; index--) {

            result = result + source.charAt(index);

        }

        return result;

    }


    /**
     * Returns a new string, starting from start and ending at end.
     * @param source  The string to reverse
     * @param start The starting position in source (inclusive)
     * @param end The end position in source (inclusive)
     * @return A new string, formed by the characters in source, starting from start to end
     * @author Ilhan Konduzov
     */
    public static String section(String source, int start, int end) {
        int size;
        String[] sourceString = source.split("");

        if (end > source.length()){
            end = source.length() - 1;
        }

        if (start < 0 || start > source.length())
        {
            start = 0;
            end = source.length() - 1;
        }

        if (start == 0){
            size = end + 1;
        } else {
            size = end - start + 1;
        }

        String[] newString = new String[size];
        int counter = 0;

        for (int i = start; i <= end; i++) {
            newString[counter] = sourceString[i];
            counter++;
        }

        String stringToReturn = "";
        for (int i = 0; i < newString.length; i++) {
            stringToReturn += newString[i];
        }
        return stringToReturn;
    }

    /**
     * Checks if the string source starts with the given character.
     * @param source The string to inspect
     * @param target The character to search for
     * @return true if string starts with target, otherwise false
     * @author Ilhan Konduzov
     */
    public static boolean startsWith(String source, char target) {
        String[] sourceString = source.split("");
        if (sourceString[0].equals(new String(new char[]{target}))){
            return true;
        }

        return false;
    }

}
