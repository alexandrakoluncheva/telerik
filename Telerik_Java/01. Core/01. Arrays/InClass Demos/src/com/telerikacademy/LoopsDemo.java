package com.telerikacademy;

public class LoopsDemo {

    public static void main(String[] args) {

        // for loop (IntelliJ: fori)
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6};
        for (int i = 0; i < numbers.length; i += 2) {
            if (numbers[i] % 2 == 0) {
                numbers[i] = 0;
            }
        }


        // for-each (IntelliJ: iter)
        String[] words = {"Banana", "Coconut"};
        for (String word : words) {
            System.out.println(word);
        }

        // while
        boolean isCool2 = true;
        while (!isCool2) {
            System.out.println("Is cool?: " + isCool2);
        }

        //do-while
        boolean isCool = true;
        do {
            System.out.println("Is cool?: " + isCool);
        } while (!isCool);

    }
}
