package com.telerikacademy;

public class PrintfDemo {

    public static void main(String[] args) {
        int codingSkillsLevel = 9003;
        boolean isCool = true;
        String ime = "Todor";

        System.out.println(ime + " is good at coding?: " + isCool + " skill level:" + codingSkillsLevel);

        System.out.printf("%s is good at coding?: %b Skill level: %d",
                ime, isCool, codingSkillsLevel);
    }

}
