package com.telerikacademy;

import java.util.Scanner;

public class ReverseArray {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String[] answer = sc.nextLine().split(" ");

        for (int i = answer.length - 1; i > answer.length / 2; i--) {
            String temporary = answer[i];
            answer[i] = answer[answer.length - 1 - i];
            answer[answer.length - 1 - i] = temporary;
        }

        System.out.println(String.join(", ", answer));

    }
}
