package com.telerikacademy;

import java.util.Scanner;


public class SymmetricArrays {
    public static void main(String[] args) {


        Scanner input = new Scanner(System.in);
        int n = Integer.parseInt(input.nextLine());
        long[][] arr = new long[n][n];
        for (int row = 0; row < n; row++) {
            arr[row][0] = (long) Math.pow(2, row);
            for (int col = 1; col < n; col++) {
                arr[row][col] = arr[row][col - 1] * 2;
            }
        }


        long sum = 0;
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row < col) sum += arr[row][col];
            }
        }


        System.out.println(sum);
    }

}