package com.telerikacademy;

public class JaggedArray {
    public static void main(String[] args) {
        int[][] jagged = new int[][]{
                new int[]{1, 7, 8, 32, 12},
                new int[]{4, 6},
                new int[]{},
                new int[]{6, 7, 3, 423, 12, 8},
                new int[]{},
                new int[10]
        };

        int numberOfRows = jagged.length;
        for (int row = 0; row < numberOfRows; row++) {

            int numberOfColumns = jagged[row].length;
            for (int col = 0; col < numberOfColumns; col++) {
                int currentElement = jagged[row][col];

                System.out.print(currentElement + " ");
            }

            System.out.println();
        }
    }
}
