package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList(Arrays.asList(new int[]{1, 2, 3}));

        myList.add(2);
        myList.add(3);
        myList.add(4);

        System.out.println(myList);

        myList.add(4, 7); // == myList.add(7);
        System.out.println(myList);

        int[] myArray = new int[]{2, 3, 4};
        System.out.println(Arrays.toString(myArray));
        myArray[1] = 7;
        System.out.println(Arrays.toString(myArray));

    }
}
