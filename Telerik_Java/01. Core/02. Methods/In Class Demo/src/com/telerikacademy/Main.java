package com.telerikacademy;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static void fakeInput() {
        String test = "3 4";

        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int userInput = scanner.nextInt();
        powerTwo(userInput);
        System.out.println("In main: " + userInput);

        int anotherUserInput = 30;
        anotherUserInput = powerThree(anotherUserInput);
        System.out.println("Another User Input: " + anotherUserInput);

        int[] array = {1, 2, 3}; // bul. Al. Malinov 31
        referenceTest(array);
        System.out.println("In main: " + Arrays.toString(array));

        int[] digitsToDouble = {1, 2, 3, 4, 5, 6};
        int[] doubledDigits = doubleAll(digitsToDouble);
        System.out.println("Original: " + Arrays.toString(digitsToDouble));
        System.out.println("Doubled: " + Arrays.toString(doubledDigits));
        System.out.printf("%s %d %n", "Todor", 18);

        System.out.println(" ******************************************* ");

        int[] result = powerAll(3, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
        System.out.println(Arrays.toString(result));
    }

    public static int[] powerAll(int power, int... numbers) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) Math.pow(numbers[i], power);
        }

        return numbers;
    }

    public static int powerThree(int base) {
        return base * base * base;
    }

    public static void powerTwo(int base) {
        base = base * base;
        System.out.println("In method: " + base);
    }

    public static int[] doubleAll(int[] digits) {
        int[] result = Arrays.copyOf(digits, digits.length);

        for (int i = 0; i < result.length; i++) {
            result[i] = result[i] * 2;
        }

        return result;
    }

    public static void referenceTest(int[] digits) {
        // bul. Al. Malinov 31[1] = 1000;
        digits[1] = 1000;
        System.out.println("In method: " + Arrays.toString(digits));
        digits = new int[]{40, 50, 60}; // jk. Druzhba
        digits[1] = 1000;
        System.out.println("In method, after reassign: " + Arrays.toString(digits));
    }
}
