package com.telerikacademy;

import java.util.Scanner;

public class BigNumbers_Original {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] arraySizes = scanner.nextLine().split(" ");
        String[] array1 = scanner.nextLine().split(" ");
        String[] array2 = scanner.nextLine().split(" ");
        int length1 = Integer.parseInt(arraySizes[0]);
        int length2 = Integer.parseInt(arraySizes[1]);
        StringBuilder resultArray = new StringBuilder();
        int tens = 0;
        int arr1Cell = 0;
        int arr2Cell = 0;
        for (int i = 0; i < Math.max(length1, length2); i++) {
            if (i < length1)
                arr1Cell = Integer.parseInt(array1[i]);
            else
                arr1Cell = 0;
            if (i < length2)
                arr2Cell = Integer.parseInt(array2[i]);
            else
                arr2Cell = 0;
            resultArray.append((arr1Cell + arr2Cell + tens) % 10);
            resultArray.append(" ");
            tens = (arr1Cell + arr2Cell + tens) / 10;
        }
        System.out.println(resultArray);
    }
}
