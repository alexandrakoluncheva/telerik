package com.telerikacademy;

import java.util.Scanner;

public class BigNumbers_Refactored {

    public static void main(String[] args) {

        String[] arraySizes = readInput();
        String[] array1 = readInput();
        String[] array2 = readInput();

        String result = sumNumbers(array1, array2);

        System.out.println(result);

    }

    public static String[] readInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().split(" ");
    }

    public static String sumNumbers(String[] array1, String[] array2) {
        int length1 = array1.length;
        int length2 = array2.length;

        StringBuilder resultArray = new StringBuilder();
        int tens = 0;
        int arr1Cell = 0;
        int arr2Cell = 0;
        for (int i = 0; i < Math.max(length1, length2); i++) {
            if (i < length1)
                arr1Cell = Integer.parseInt(array1[i]);
            else
                arr1Cell = 0;
            if (i < length2)
                arr2Cell = Integer.parseInt(array2[i]);
            else
                arr2Cell = 0;
            resultArray.append(extractCarry(arr1Cell, arr2Cell, tens));
            resultArray.append(" ");
            tens = (arr1Cell + arr2Cell + tens) / 10;
        }

        return resultArray.toString();
    }

    public static int extractCarry(int num1, int num2, int tens) {
        return (num1 + num2 + tens) % 10;
    }
}
