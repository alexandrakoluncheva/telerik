package com.telerikacademy;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class SpiralMatrix {
    private static void fakeInput() {
        String test = "100";

        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    public static void main(String[] args) {
        fakeInput();

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];

        int maxRow = n - 1;
        int minRow = 0;
        int maxCol = n - 1;
        int minCol = 0;

        int lastElement = 0;

        while (maxCol >= 0 && maxRow >= 0 && minCol <= n && minRow <= n) {
            lastElement = getTopmostRow(matrix, minRow, maxCol, minCol, lastElement);
            minRow++;

            lastElement = getRightmostColumn(matrix, maxRow, minRow, maxCol, lastElement);
            maxCol--;

            lastElement = getBottommostRow(matrix, maxRow, maxCol, minCol, lastElement);
            maxRow--;

            lastElement = getLeftmostColumn(matrix, maxRow, minRow, minCol, lastElement);
            minCol++;
        }

        print(matrix);

    }

    private static void print(int[][] matrix) {
        for (int[] row : matrix) {
            for (int cell : row) {
                //souf
                System.out.printf("%d ", cell);
            }
            System.out.println();
        }
    }

    private static int getLeftmostColumn(int[][] matrix, int maxRow, int minRow, int minCol, int lastElement) {
        for (int i = maxRow; i >= minRow; i--) {
            matrix[i][minCol] = ++lastElement;
        }
        return lastElement;
    }

    private static int getBottommostRow(int[][] matrix, int maxRow, int maxCol, int minCol, int lastElement) {
        for (int i = maxCol; i >= minCol; i--) {
            matrix[maxRow][i] = ++lastElement;
        }
        return lastElement;
    }

    private static int getRightmostColumn(int[][] matrix, int maxRow, int minRow, int maxCol, int lastElement) {
        for (int i = minRow; i <= maxRow; i++) {
            matrix[i][maxCol] = ++lastElement;
        }
        return lastElement;
    }

    private static int getTopmostRow(int[][] matrix, int minRow, int maxCol, int minCol, int lastElement) {
        for (int i = minCol; i <= maxCol; i++) {//todo
            matrix[minRow][i] = ++lastElement;
        }
        return lastElement;
    }
}
