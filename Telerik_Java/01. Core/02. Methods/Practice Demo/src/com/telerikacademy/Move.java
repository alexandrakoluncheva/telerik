package com.telerikacademy;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Move {
    private static void fakeInput() {
        String test = "0\n" +
                "10,20\n" +
                "1 forward 1\n" +
                "2 backwards 4\n" +
                "1 forward 1\n" +
                "exit";

//        Forward: 150
//        Backwards: 120

        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    public static void main(String[] args) throws IOException {
        fakeInput();
        // Scanner scanner = new Scanner(System.in);
        // scanner.nextInt();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int currentPosition = Integer.parseInt(reader.readLine());

        String[] rawInput = reader.readLine().split(",");
        int[] values = convertToIntArray(rawInput);

        String[] command = {""};

        int forwardSum = 0, backwardsSum = 0, times = 0, sizeOfJump = 0;

        while (true) {
            command = reader.readLine().split(" ");

            if (command[0].equals("exit")) break;

            times = Integer.parseInt(command[0]);

            sizeOfJump = Integer.parseInt(command[2]);

            switch (command[1]) {
                case "forward": {
                    for (int i = 0; i < times; i++) {
                        currentPosition = (currentPosition + sizeOfJump) % values.length;
                        forwardSum += values[currentPosition];
                    }
                    break;
                }
                case "backwards": {
                    for (int i = 0; i < times; i++) {
                        currentPosition = currentPosition - sizeOfJump;
                        while (currentPosition < 0) {
                            currentPosition += values.length;
                        }
                        backwardsSum += values[currentPosition];
                    }
                }
                default:
                    break;
            }

        }

        System.out.printf("Forward: %d%n", forwardSum);
        System.out.printf("Backwards: %d%n", backwardsSum);

    }

    public static int[] convertToIntArray(String[] arrayToConvert) {
        int[] result = new int[arrayToConvert.length];

        for (int i = 0; i < arrayToConvert.length; i++) {
            result[i] = Integer.parseInt(arrayToConvert[i]);
        }

        return result;
    }
}
