package com.telerikacademy;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrimeNumbers {
    private static void fakeInput() {
        String test = "15";

        System.setIn(new ByteArrayInputStream(test.getBytes()));
    }

    public static void main(String[] args) throws IOException {
        fakeInput();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int until = Integer.parseInt(br.readLine());

        for (int current = 1; current <= until; current++) {
            if (isPrime(current)) {
                System.out.print(current + " ");
            }
        }
    }

    public static boolean isPrime(int number) {
        if (number == 1) return true;

        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) return false;
        }

        return true;
    }
}
