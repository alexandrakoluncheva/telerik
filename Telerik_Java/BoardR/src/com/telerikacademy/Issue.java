package com.telerikacademy;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private String description;
public String getDescription(){return description;}
    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);

        if (description == null) {
            this.description = "No description";
        } else {
            this.description = description;
        }
    }

    void setStatus(Status status) {
        this.logEvent(String.format("Issue changed from %s to %s", this.getStatus(), status));

        this.status = status;
    }


    @Override
    public void revertStatus() {
        Status initialStatus=Status.OPEN;
        if (this.status != initialStatus) {
            setStatus(Status.OPEN);
        } else {
            this.logEvent(String.format("Issue status is already %s", this.getStatus()));
        }
    }




    @Override
    public void advanceStatus() {
        Status finalStatus=Status.VERIFIED;
        if (this.status != finalStatus) {
            setStatus(Status.VERIFIED);
        } else {
            this.logEvent(String.format("Issue status is already %s", this.getStatus()));
        }
    }

    @Override
    public String viewInfo() {
        // get the common info
        String baseInfo = super.viewInfo();

        // add additional info, based on which subclass you are in
        return String.format("Issue: %s, Description: %s", baseInfo, this.getDescription());
    }


}

