package com.telerikacademy;

import java.time.LocalDate;

public class Task extends BoardItem {

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, Status.TODO);

        ensureValidAssignee(assignee);
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        ensureValidAssignee(assignee);

        logEvent(String.format("Assignee changed from %s to %s", this.getAssignee(), assignee));

        this.assignee = assignee;
    }

    private void ensureValidAssignee(String assignee) {
        if (assignee.length() < 5 || assignee.length() > 30) {
            throw new IllegalArgumentException("The assignee's name must be between 5 and 30 characters.");
        }
    }

    void setStatus(Status status) {
        this.logEvent(String.format("Task changed from %s to %s", this.getStatus(), status));

        this.status = status;
    }

    @Override
    public void advanceStatus() {
        Status finalStatus=Status.VERIFIED;
        if (this.status != finalStatus) {
            setStatus(Status.values()[status.ordinal() + 1]);
        } else {
            this.logEvent(String.format("Can't advance, already at %s", this.getStatus()));
        }
    }

    @Override
    public void revertStatus() {
        Status initialStatus=Status.OPEN;
        if (this.status != initialStatus) {
            setStatus(Status.values()[status.ordinal() - 1]);
        } else {
            this.logEvent(String.format("Can't revert, already at %s", this.getStatus()));
        }
    }

    @Override
    public String viewInfo() {
        // get the common info
        String baseInfo = super.viewInfo();

        // add additional info, based on which subclass you are in
        return String.format("Task: %s, Assignee: %s", baseInfo, this.getAssignee());
    }


}
