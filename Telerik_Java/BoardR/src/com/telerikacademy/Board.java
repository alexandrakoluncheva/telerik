package com.telerikacademy;

import loggers.ConsoleLogger;
import loggers.Logger;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private final static List<BoardItem> items = new ArrayList<>();

    private Board() {
    }

    public static void addItem(BoardItem item) {
        if (items.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }

        items.add(item);
    }

    public static void displayHistory(Logger logger) { // accept an Logger type
        for (BoardItem item : items) {
            // call the log() method and give it a string. (the viewHistory() method returns a String)
            logger.log(item.getHistory());
        }
    }


    public static int totalItems() {
        return items.size();
    }

}
