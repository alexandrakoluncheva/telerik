package com.telerikacademy;

public class Main {

    public static void main(String[] args) {
	    User tosho = new User();
        User pesho = new User("Pesho", 1234);
        pesho.setUsername("Gosho");
	    User gosho = pesho;

        System.out.println(pesho.logIn());
        System.out.println(pesho.username);
//        tosho.setUsername(null); //error
    }
}
