package com.telerikacademy;

public class User {
    String username;
    int password;

    public User() {
        this("No name");
    }

    public User(String username) {
        this(username, 0);
    }

    public User(String username, int pass) {
        setUsername(username);
        password = pass;
    }

    public void setUsername(String username) {
        if (username == null) {
            throw new IllegalArgumentException("Username cannot be null");
        }
        if (username.length() > 20 || username.length() < 3) {
            throw new IllegalArgumentException("Username should be between 3 and 20 characters");
        }
        this.username = username;
    }

    public String logIn() {
        return String.format("%s - %d", username, password);
    }
}
